Pod::Spec.new do |s|
  s.name = "AdobeCreativeSDKImage"
  s.version = "0.0.1"
  s.summary = "Adobe CreativeSDK for iOS Image Dynamic Framework."
  s.description = "Adobe CreativeSDK iOS Frameworks."
  s.homepage = "https://creativesdk.adobe.com/"
  s.license = {
    :type => "Commercial"
  }
  s.authors = {
    "SoulInCode" => "hello@sic.studio"
  }
  s.documentation_url = "https://creativesdk.adobe.com/docs/ios/#/index.html"
  s.source = {
    "git": "git@bitbucket.org:krush42/adobecreativesdk.git"
  }
  s.platform = :ios, "8.0"
  s.libraries =  "z", "sqlite3"
  s.requires_arc = true
  s.source_files = "AdobeImageLib/AdobeCreativeSDKImage.framework/Versions/A/Headers/AdobeCreativeSDKImage.h"
  s.resources = "AdobeImageLib/AdobeCreativeSDKImage.framework/Versions/A/Resources/AdobeCreativeSDKImageResources.bundle"
  s.frameworks = "AdobeCreativeSDKImage", "UIKit", "WebKit", "OpenGLES", "QuartzCore", "StoreKit", "SystemConfiguration", "MessageUI", "CoreData", "Foundation"
  s.preserve_paths = "*"
  s.xcconfig = { "FRAMEWORK_SEARCH_PATHS" => "$(PODS_ROOT)/AdobeCreativeSDKImage/AdobeImageLib/" }
end
